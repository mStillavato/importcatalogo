package net.codejava.networking;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import net.viralpatel.java.CSVLoader;

public class HttpDownloader {

	private static String JDBC_CONNECTION_URL = 
			"jdbc:oracle:thin:OMP_PORTLET/OMP_PORTLET@10.88.91.97:1521:OMED";
			//"jdbc:oracle:thin:OMP_PORTLET/OMP_PORTLET@localhost:1521:ORCL";
	
	private static String FILE_CATALOGO = 
			"/opt/catalogo/Inforecord.csv";
			//"C:/Users/m.stillavato/Documents/Vodafone/OMP/Catalogo/Inforecord.csv";
	
	public static void main(String[] args) {
		
		try {
			CSVLoader loader = new CSVLoader(getCon());
			loader.loadCSV(FILE_CATALOGO, "CATALOGO", true);
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static Connection getCon() {
		Connection connection = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			connection = DriverManager.getConnection(JDBC_CONNECTION_URL);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return connection;
	}
}